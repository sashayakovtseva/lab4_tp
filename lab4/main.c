#include <stdio.h>
#include <sqlite3.h>
#include "db_interface.h"


sqlite3 *db;

int main(int argc, const char * argv[])
{
    if(sqlite3_open("employees.db", &db) != SQLITE_OK)
    {
        fprintf(stderr, "Cannot open DB: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return 1;
    }
    

    int n = 5, i;
    int choice;
    char *menu[] =
    {
        "Вывести всю информацию о сотрудниках в файл",
        "Ввести новые данные",
        "Удалить данные",
        "Произвести выборку иформации",
        "Выход из приложения"
    };
    
    do
    {
        for(i = 1; i <= n; i++)
        {
            printf("%d. %s\n", i, menu[i - 1]);
        }
        printf("Ваш выбор: ");
        scanf("%d", &choice);
        
        switch (choice)
        {
            case 1:
                outputAllData();
                break;
                
            case 2:
                insertData();
                break;
                
            case 3:
                deleteData();
                break;
                
            case 4:
                selectData();
                break;
                
            case 5:
                sqlite3_close(db);
                return 0;
                
            default:
                printf("Неизвестная операция: %d\n", choice);
        }
    
        printf("\n");
    } while (1);
    

    return 0;
}
