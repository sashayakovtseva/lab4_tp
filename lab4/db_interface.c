#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "db_interface.h"

extern sqlite3 *db;
FILE *fout;

int outputDataToFile(void *notUsed, int argc, char **argv, char **azColName)
{
    FILE *img;
    int i;
    char *sql_template = "select photo from employees where id = ";
  
    for(i = 0; i < argc; i++)
    {
        if(!strcmp("photo", azColName[i]))
        {
            char sql[100] = {0};
            int imgExists = 0;
            int bytes = 0;
            
            sqlite3_stmt *pStmt;
            sprintf(sql, "%s%s;", sql_template, argv[0]);
          
            if(sqlite3_prepare_v2(db, sql, -1, &pStmt, 0) != SQLITE_OK)
            {
                fprintf(stderr, "Failed to prepare statement.\n");
                fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
                return SQLITE_ERROR;
            }
            
            
            if(sqlite3_step(pStmt) == SQLITE_ROW)
            {
                bytes = sqlite3_column_bytes(pStmt, 0);
            }
            
            if(sqlite3_column_type(pStmt, 0) != SQLITE_NULL)
            {
                imgExists = 1;
                char img_name[15] = {0};
                
                strcat(img_name, argv[0]);
                strcat(img_name, "_photo.png");
                img = fopen(img_name, "wb");
                
                
                if(img == NULL)
                {
                    fprintf(stderr, "Cannot open image file!\n");
                }
                fwrite(sqlite3_column_blob(pStmt, 0), bytes, 1, img);
                fclose(img);
                
                fprintf(fout, "%s = %s\n", azColName[i], img_name);
                
            }
            sqlite3_finalize(pStmt);
            if(imgExists)
                continue;
        }
        
        
        fprintf(fout, "%s = %s\n", azColName[i],(argv[i] ? argv[i] : "NULL"));

    }
    
                
    fprintf(fout, "\n");
    return 0;
}

void outputAllData()
{
    char *err_msg = 0;
    const char *sql = "select * from employees;";
    fout = fopen("employees_out.txt", "w");
    
    if(sqlite3_exec(db, sql, outputDataToFile, 0, &err_msg) != SQLITE_OK)
    {
       fprintf(stderr,"Failed to select data.\n");
       fprintf(stderr,"SQL error: %s\n", err_msg);
        
        sqlite3_free(err_msg);
    }
    fclose(fout);
    
}

void insertAutocommit()
{
    struct emp newEmp;
    char sql[256];
    char *errmsg = 0;
    
    getchar();
    printf("Введите имя нового сотрудника: ");
    gets(newEmp.firstname);
    printf("Введите фамилию нового сотрудника: ");
    gets(newEmp.lastname);
    printf("Введите дату рождения нового сотрудника: ");
    gets(newEmp.birthdate);
    printf("Введите город нового сотрудника: ");
    gets(newEmp.city);
    printf("Введите страну нового сотрудника: ");
    gets(newEmp.country);
    printf("Введите адрес нового сотрудника: ");
    gets(newEmp.address);
    printf("Введите должность нового сотрудника: ");
    gets(newEmp.position);
    printf("Введите отдел нового сотрудника: ");
    gets(newEmp.dept);
    printf("Введите дату начала работы нового сотрудника: ");
    gets(newEmp.startdate);
    
    
    sprintf(sql, "insert into employees(firstname, lastname, birthdate, "
            "city, country, address, position, dept, startdate) values "
            "( \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');",
            newEmp.firstname, newEmp.lastname, newEmp.birthdate, newEmp.city, newEmp.country,
            newEmp.address, newEmp.position, newEmp.dept, newEmp.startdate);
    
    if(sqlite3_exec(db, sql, 0, 0, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to insert data.\n");
        fprintf(stderr, "SQL error: %s\n", errmsg);
        
        sqlite3_free(errmsg);
    }
    
}

void insertTransaction()
{
    struct emp newEmp;
    char sql[256];
    char *errmsg = 0;
    
    getchar();
    printf("Введите имя нового сотрудника: ");
    gets(newEmp.firstname);
    printf("Введите фамилию нового сотрудника: ");
    gets(newEmp.lastname);
    printf("Введите дату рождения нового сотрудника: ");
    gets(newEmp.birthdate);
    printf("Введите город нового сотрудника: ");
    gets(newEmp.city);
    printf("Введите страну нового сотрудника: ");
    gets(newEmp.country);
    printf("Введите адрес нового сотрудника: ");
    gets(newEmp.address);
    printf("Введите должность нового сотрудника: ");
    gets(newEmp.position);
    printf("Введите отдел нового сотрудника: ");
    gets(newEmp.dept);
    printf("Введите дату начала работы нового сотрудника: ");
    gets(newEmp.startdate);
    
    
    sprintf(sql, "begin transaction;"
            "insert into employees(firstname, lastname, birthdate, "
            "city, country, address, position, dept, startdate) values "
            "( \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\');"
            "commit;",
            newEmp.firstname, newEmp.lastname, newEmp.birthdate, newEmp.city, newEmp.country,
            newEmp.address, newEmp.position, newEmp.dept, newEmp.startdate);
    
    if(sqlite3_exec(db, sql, 0, 0, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to insert data.\n");
        fprintf(stderr, "SQL error: %s\n", errmsg);
        
        sqlite3_free(errmsg);
    }
}

void insertData()
{
    int choice;
    int n = 2, i;
    const char *menu[] =
    {
        "В режиме autocommit",
        "В режиме транзакци"
    };

    printf("\n");
    for(i = 1; i <= n; i++)
    {
        printf("\t%d.%s\n",i , menu[i - 1]);
    }
    printf("Ваш выбор: ");
    scanf("%d", &choice);
    
    switch (choice)
    {
        case 1:
            insertAutocommit();
            break;
            
        case 2:
            insertTransaction();
            break;
            
        default:
           printf("Неизвестная операция: %d\n", choice);
    }
    
}

void deleteData()
{
    int idToDelete;
    char sql[100];
    char *errmsg = 0;
    
    printf("Введите id для удаления: ");
    scanf("%d", &idToDelete);
    sprintf(sql, "delete from employees where id  = %d", idToDelete);
    
    if(sqlite3_exec(db, sql, 0, 0, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to insert data.\n");
        fprintf(stderr, "SQL error: %s\n", errmsg);
        
        sqlite3_free(errmsg);
    }
    
}

int outputDataToStdout(void *notUsed, int argc, char **argv, char **azColName)
{
    int i;
    for(i = 0; i < argc; i++)
    {
        
        if(!strcmp("photo", azColName[i]))
        {
            char img_name[15] = {0};
            
            strcat(img_name, argv[0]);
            strcat(img_name, "_photo.png");
            
            if(argv[i] == NULL)
                strcpy(img_name, "NULL");
            
            fprintf(stdout, "%s = %s\n", azColName[i], img_name);

        }
        else
            fprintf(stdout, "%s = %s\n", azColName[i],(argv[i] ? argv[i] : "NULL"));
        
    }
    
    
    fprintf(stdout, "\n");
    return 0;

}

void selectById()
{
    char sql[100];
    char param[10];
    char *errmsg = 0;
    
    getchar();
    printf("Введите параметр для запроса (например, < 5): ");
    gets(param);
    printf("\n");
    sprintf(sql, "select * from employees where id %s;", param);
    
    if(sqlite3_exec(db, sql, outputDataToStdout, 0, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to select data.\n");
        fprintf(stderr, "SQL error: %s\n", errmsg);
        
        sqlite3_free(errmsg);

    }
    
}
void selectByLastname()
{
    char sql[100];
    char param[10];
    char *errmsg = 0;
    
    getchar();
    printf("Введите параметр для запроса (например, %%eith%%): ");
    gets(param);
    printf("\n");
    sprintf(sql, "select * from employees where lastname like \'%s\';", param);
    
    if(sqlite3_exec(db, sql, outputDataToStdout, 0, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to select data.\n");
        fprintf(stderr, "SQL error: %s\n", errmsg);
        
        sqlite3_free(errmsg);
        
    }
}
void selectByCountry()
{
    char sql[100];
    char param[10];
    char *errmsg = 0;
    
    getchar();
    printf("Введите параметр для запроса (например, %%aus%%): ");
    gets(param);
    printf("\n");
    sprintf(sql, "select * from employees where country like \'%s\';", param);
    
    if(sqlite3_exec(db, sql, outputDataToStdout, 0, &errmsg) != SQLITE_OK)
    {
        fprintf(stderr, "Failed to select data.\n");
        fprintf(stderr, "SQL error: %s\n", errmsg);
        
        sqlite3_free(errmsg);
        
    }

}

void selectData()
{
    int choice;
    int n = 3, i;
    const char *menu[] =
    {
        "По id",
        "По фамилии",
        "По стране проживания"
    };
    
    printf("\n");
    for(i = 1; i <= n; i++)
    {
        printf("\t%d.%s\n",i , menu[i - 1]);
    }
    printf("Ваш выбор: ");
    scanf("%d", &choice);
    
    switch (choice)
    {
        case 1:
            selectById();
            break;
            
        case 2:
            selectByLastname();
            break;
            
        case 3:
            selectByCountry();
            break;
            
        default:
            printf("Неизвестная операция: %d\n", choice);
    }
   
}
