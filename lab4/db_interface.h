#include <sqlite3.h>

#ifndef db_interface_h
#define db_interface_h

struct emp
{
    char firstname[100];
    char lastname[100];
    char city[100];
    char country[100];
    char address[100];
    char position[100];
    char dept[100];
    char birthdate[100];
    char startdate[100];
    
};

void outputAllData();
void insertData();
void deleteData();
void selectData();


#endif /* db_interface_h */
